# aws-signing-proxy

[![pipeline status](https://gitlab.com/msvechla/aws-signing-proxy/badges/master/pipeline.svg)](https://gitlab.com/msvechla/aws-signing-proxy/commits/master) [![coverage report](https://gitlab.com/msvechla/aws-signing-proxy/badges/master/coverage.svg)](https://gitlab.com/msvechla/aws-signing-proxy/commits/master) [![go report](https://goreportcard.com/badge/gitlab.com/msvechla/aws-signing-proxy)](https://goreportcard.com/report/gitlab.com/msvechla/aws-signing-proxy) [![Docker Pulls](https://badgen.net/docker/pulls/msvechla/aws-signing-proxy)](https://hub.docker.com/r/msvechla/aws-signing-proxy)

*Easily connect to secured AWS services like Elasticsearch via the aws-signing-proxy. Works both locally and in production.*

<!-- TOC -->

- [Overview](#overview)
- [Quickstart](#quickstart)
    - [Forward Proxy Mode](#forward-proxy-mode)
    - [Reverse Proxy Mode](#reverse-proxy-mode)
    - [Installing via Homebrew](#installing-via-homebrew)
    - [Running in Docker](#running-in-docker)
    - [Building from Source](#building-from-source)
- [Configuration Options](#configuration-options)
- [Contributing](#contributing)
- [Versioning](#versioning)
- [Authors](#authors)
- [License](#license)

<!-- /TOC -->

## Overview

Many secured AWS services require you to sign your requests via the AWS SDK. Many open source tools do not support this requirement. Additionally this makes it difficult to explore some of the APIs locally via well known tools, such as curl.

The `aws-signing-proxy` solves this issue, by acting as a forward- or reverse proxy, that will sign all requests passing through with IAM credentials.

## Quickstart

Choose one of the following installation options:

- [homebrew](#installing-via-homebrew)
- [docker](#running-in-docker)
- [build from source](#building-from-source)

Afterwards your are ready to connect to secured services via the proxy.

### Forward Proxy Mode

By default `aws-signing-proxy` will run in forward proxy mode:

```sh
❯ ./aws-signing-proxy --service es --region eu-central-1
INFO[0000] listening for incoming requests on localhost:9090 
INFO[0038] proxy request served                          code=200 durationTotalMS=397 host=my-elasticsearch.eu-central-1.es.amazonaws.com method=GET sizeBytes=128 src="127.0.0.1:50093" url="http://my-elasticsearch.eu-central-1.es.amazonaws.com/_cat/indices" userAgent=curl/7.64.1
```

The proxy will then sign request with the AWS credentials discovered via the [default credential provider chain](https://docs.aws.amazon.com/sdk-for-go/v1/developer-guide/configuring-sdk.html#specifying-credentials).

You can now set this proxy in any tool and the requests will get signed automatically:

```sh
curl http://my-elasticsearch.eu-central-1.es.amazonaws.com/_cat/indices -x http://localhost:9090
green open test      rx8bP2qKTt-O7qvaJQUamA 5 1 0 0 2.7kb 1.3kb
```

To increase the usability, you can also simply set the `http_proxy` environment variable via `export http_proxy=http://localhost:9090` and use supported tools like `curl` as usual.

> ⚠️ keep in mind that while `aws-signing-proxy` only handles incoming plain HTTP calls, it will always connect to the outside via HTTPS / TLS by default. Check the CLI options for more details.

### Reverse Proxy Mode

When specifying a `--target` option, `aws-signing-proxy` operates in reverse-proxy mode.

In this mode, all requests directed at the proxy will get proxied to the specified target. This can be useful for certain tools that do not support the usual `HTTP_PROXY` environment variables, or configuring forward proxies in general.

```sh
./aws-signing-proxy --target https//my-elasticsearch.eu-central-1.es.amazonaws.com
INFO[0000] listening for incoming requests on localhost:9090 
INFO[0000] running in [reverse proxy mode], as --target https//my-elasticsearch.eu-central-1.es.amazonaws.com configured, 
INFO[0005] proxy request served                          code=200 durationTotalMS=413 host="localhost:9090" method=GET sizeBytes=94 src="127.0.0.1:50312" url=/_cat/health userAgent=curl/7.64.1
```

You can now send requests directly to the proxy and the requests will get proxies transparently to the target:

```sh
curl localhost:9090/_cat/health           
1594150363 19:32:43 810640839231:test green 2 2 true 12 6 0 0 0 0 - 100.0%
```

### Installing via Homebrew

`aws-signing-proxy` can be installed via Homebrew on MacOS via these simple steps:

```sh
brew tap msvechla/aws-signing-proxy https://gitlab.com/msvechla/aws-signing-proxy
brew install msvechla/aws-signing-proxy/aws-signing-proxy
```

### Running in Docker

You can run `aws-signing-proxy` in Docker as well, by either mounting your AWS credentials or exposing the AWS environment variables:

- mounting AWS credentials:

```sh
docker run -it -v $HOME/.aws/credentials:/home/app/.aws/credentials -p 9090:9090 msvechla/aws-signing-proxy --host 0.0.0.0  --service es --region eu-central-1
curl http://my-elasticsearch.eu-central-1.es.amazonaws.com/_cat/indices -x http://localhost:9090
```

- using environment variables:

```sh
docker run -it -e AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY -p 9090:9090 msvechla/aws-signing-proxy --host 0.0.0.0  --service es --region eu-central-1
curl http://my-elasticsearch.eu-central-1.es.amazonaws.com/_cat/indices -x http://localhost:9090
```

### Building from Source

The latest release can be built from source by following these instructions:

```sh
git clone https://gitlab.com/msvechla/aws-signing-proxy.git
cd aws-signing-proxy
go build .
```

## Configuration Options

`aws-signing-proxy` has the following configuration options:

```sh
GLOBAL OPTIONS:
   --host value                 host the proxy should listen on (default: "localhost") [$HOST]
   --port value                 port the proxy should listen on (default: 9090) [$PORT]
   --service value              AWS service the requests should be signed for (default: "es") [$SERVICE]
   --target value               if set, aws-signing-proxy will act as a reverse proxy and rewrite requests to the specified target in the form of: proto://host:port [$TARGET]
   --region value               AWS region the requests should be signed for (default: "eu-central-1") [$REGION]
   --disable-https-rewrite      when set, disables the default behaviour of the proxy to re-write every http request to https before connecting to the target (default: false) [$DISABLE_HTTPS_REWRITE]
   --disable-credentials-check  when set, disables the verification of the supplied AWS credentials during startup (default: false) [$DISABLE_CREDENTIALS_CHECK]
   --help, -h                   show help (default: false)
   --version, -v                print the version (default: false)
```

## Contributing

Please read [CONTRIBUTING.md]() for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/msvechla/aws-signing-proxy/tags) or take a look at the [CHANGELOG.md](./CHANGELOG.md)

## Authors

- **Marius Svechla** - *Initial work*

See also the list of [contributors](https://gitlab.com/msvechla/aws-signing-proxy/graphs/master) who participated in this project.

## License

[MIT License](./LICENSE.md)  
Copyright (c) 2020 Marius Svechla
