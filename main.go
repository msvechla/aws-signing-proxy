package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"

	"gitlab.com/msvechla/aws-signing-proxy/cmd"
)

const (
	exitErr       = 1
	exitInterrupt = 2
)

func main() {
	// create context and listen for interrupt signals
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, os.Interrupt)

	defer func() {
		signal.Stop(signalChan)
		cancel()
	}()

	go func() {
		select {
		case <-signalChan:
			cancel()
		case <-ctx.Done():
		}

		// on second interrupt, exit immediately
		<-signalChan
		os.Exit(exitInterrupt)
	}()

	if err := run(ctx); err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(exitErr)
	}
}

func run(c context.Context) error {
	app := cmd.InitializeApp(c)
	return app.Run(os.Args)
}
