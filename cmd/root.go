package cmd

import (
	"context"

	"github.com/urfave/cli/v2"
	"gitlab.com/msvechla/aws-signing-proxy/internal/pkg/proxy"
)

const appVersion = "v0.1.0"

// InitializeApp initializes the CLI
func InitializeApp(appContext context.Context) *cli.App {
	config := proxy.Config{}

	return &cli.App{
		Authors: []*cli.Author{{
			Name:  "Marius Svechla",
			Email: "m.svechla@gmail.com"},
		},
		Usage:   "forward proxy that signs requests to AWS services",
		Version: appVersion,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "host",
				EnvVars:     []string{"HOST"},
				Value:       "localhost",
				Usage:       "host the proxy should listen on",
				Destination: &config.Host,
			},
			&cli.IntFlag{
				Name:        "port",
				EnvVars:     []string{"PORT"},
				Value:       9090,
				Usage:       "port the proxy should listen on",
				Destination: &config.Port,
			},
			&cli.StringFlag{
				Name:        "service",
				EnvVars:     []string{"SERVICE"},
				Value:       "es",
				Usage:       "AWS service the requests should be signed for",
				Destination: &config.Service,
			},
			&cli.StringFlag{
				Name:        "target",
				EnvVars:     []string{"TARGET"},
				Value:       "",
				Usage:       "if set, aws-signing-proxy will act as a reverse proxy and rewrite requests to the specified target in the form of: proto://host:port",
				Required:    false,
				Destination: &config.Target,
			},
			&cli.StringFlag{
				Name:        "region",
				EnvVars:     []string{"REGION"},
				Value:       "eu-central-1",
				Usage:       "AWS region the requests should be signed for",
				Destination: &config.Region,
			},
			&cli.BoolFlag{
				Name:        "disable-https-rewrite",
				EnvVars:     []string{"DISABLE_HTTPS_REWRITE"},
				Value:       false,
				Usage:       "when set, disables the default behaviour of the proxy to re-write every http request to https before connecting to the target",
				Destination: &config.DisableHTTPSRewrite,
			},
			&cli.BoolFlag{
				Name:        "disable-credentials-check",
				EnvVars:     []string{"DISABLE_CREDENTIALS_CHECK"},
				Value:       false,
				Usage:       "when set, disables the verification of the supplied AWS credentials during startup",
				Destination: &config.DisableCredentialsCheck,
			},
		},
		Action: func(c *cli.Context) error {
			return run(appContext, &config)
		},
	}
}

func run(appContext context.Context, config *proxy.Config) error {
	proxy, err := proxy.NewProxy(appContext, config)
	if err != nil {
		return err
	}

	return proxy.ListenAndServe()
}
