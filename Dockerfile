## Builder Image
FROM golang:1 as builder
RUN apt update && apt install -y --no-install-recommends --upgrade openssl
WORKDIR /aws-signing-proxy

# Copy go mod first and download all modules so layers will be cached for faster local builds
COPY go.mod .
COPY go.sum .
RUN go mod download

COPY / .
RUN CGO_ENABLED=0 GO111MODULE=on GOOS=linux go build -a -installsuffix cgo -o aws-signing-proxy .

## Application Image
FROM alpine:latest

RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*

RUN mkdir -p /home/app && \
    addgroup app && \
    adduser -D -G app app

WORKDIR /home/app
COPY --from=builder /aws-signing-proxy/aws-signing-proxy .
RUN chown -R app:app /home/app
USER app
ENTRYPOINT ["./aws-signing-proxy"]
