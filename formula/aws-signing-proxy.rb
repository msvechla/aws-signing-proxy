class AwsSigningProxy < Formula
  desc "Easily connect to secured AWS services like Elasticsearch via the aws-signing-proxy. Works both locally and in production."
  homepage "https://gitlab.com/msvechla/aws-signing-proxy"
  url "https://msvechla.gitlab.io/aws-signing-proxy/aws-signing-proxy-darwin-amd64.tar.gz"
  version "v0.1.0"
  sha256 "f9b0c3df8c2c653b2363d72a3356780c43443ab26a39e6a0d3b3402d24109669"

  def install
    bin.install "aws-signing-proxy"
  end

  test do
    assert_match "aws-signing-proxy version v0.1.0", shell_output("#{bin}/aws-signing-proxy -v", 0)
  end
end
