module gitlab.com/msvechla/aws-signing-proxy

go 1.14

require (
	github.com/aws/aws-sdk-go v1.32.8
	github.com/sirupsen/logrus v1.6.0
	github.com/stretchr/testify v1.5.1
	github.com/urfave/cli/v2 v2.2.0
	golang.org/x/sys v0.0.0-20200622214017-ed371f2e16b4 // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
