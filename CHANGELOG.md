# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v0.1.0] - 2020-07-07

### Added

- new flag `-target` to run in reverse proxy mode
- improved logging
- environment variable configuration support

## [v0.0.1] - 2020-07-01

### Added

- initial release
- cli
- forward proxy
- rewrite requests to https
- lots more 🚀
