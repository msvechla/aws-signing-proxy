package proxy

import (
	"bytes"
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	log "github.com/sirupsen/logrus"
)

func TestResponseWriter(t *testing.T) {
	sResponseWriter := statusResponseWriter{}

	handler := func(w http.ResponseWriter, r *http.Request) {
		sResponseWriter.ResponseWriter = w
		sResponseWriter.WriteHeader(http.StatusBadGateway)
	}

	req := httptest.NewRequest("GET", "http://example.com/foo", nil)
	w := httptest.NewRecorder()
	handler(w, req)

	assert.Equal(t, sResponseWriter.status, http.StatusBadGateway)
}

func TestCaptureHTTPLog(t *testing.T) {
	logger := log.New()

	out := &bytes.Buffer{}
	logger.SetOutput(out)

	p := proxy{
		context: context.Background(),
		logger:  logger,
	}

	url := "http://example.com/foo"

	req := httptest.NewRequest("GET", url, nil)
	w := httptest.NewRecorder()

	p.loggingMiddleware(func(w http.ResponseWriter, r *http.Request) {}).ServeHTTP(w, req)

	logLine := fmt.Sprintf("%s", out)
	assert.Contains(t, logLine, url)
}
