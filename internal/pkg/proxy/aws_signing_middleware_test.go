package proxy

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strings"
	"testing"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func TestAWSSigningMiddlewareForwardProxy(t *testing.T) {
	p := proxy{
		context: context.Background(),
		config:  &Config{Host: "localhost", Port: 8090, Service: "es", Region: "eu-central-3", DisableCredentialsCheck: true, DisableHTTPSRewrite: false},
	}

	testHandler := func(w http.ResponseWriter, r *http.Request) {
		fmt.Println(r)
		assert.Contains(t, r.Header.Get("Authorization"), "eu-central-3")
		assert.NotEmpty(t, r.Header.Get("X-Amz-Security-Token"))
		assert.NotEmpty(t, r.Header.Get("X-Amz-Date"))
		assert.Equal(t, "https", r.URL.Scheme)
		assert.Equal(t, "example.com", r.URL.Host)
	}

	req, _ := http.NewRequest("POST", "http://example.com/foo", strings.NewReader("helloWorld"))
	w := httptest.NewRecorder()

	// set test aws credentials
	os.Setenv("AWS_ACCESS_KEY_ID", "TEST_ID")
	os.Setenv("AWS_SECRET_ACCESS_KEY", "TEST_KEY")
	os.Setenv("AWS_SECURITY_TOKEN", "TESTSECTOKEN")
	os.Setenv("AWS_SESSION_TOKEN", "TESTSESTOKEN")

	p.awsSigningMiddleware(testHandler).ServeHTTP(w, req)
}

func TestAWSSigningMiddlewareReverseProxy(t *testing.T) {
	rawURL := "http://localhost:1234"
	targetURL, err := url.Parse(rawURL)
	assert.NoError(t, err)

	p := proxy{
		context:   context.Background(),
		config:    &Config{Host: "localhost", Port: 8090, Service: "es", Region: "eu-central-3", Target: rawURL, DisableCredentialsCheck: true, DisableHTTPSRewrite: false},
		targetURL: targetURL,
		logWriter: log.New().Writer(),
	}

	testHandler := func(w http.ResponseWriter, r *http.Request) {
		assert.Contains(t, r.Header.Get("Authorization"), "eu-central-3")
		assert.NotEmpty(t, r.Header.Get("X-Amz-Security-Token"))
		assert.NotEmpty(t, r.Header.Get("X-Amz-Date"))
		assert.Equal(t, "https", r.URL.Scheme)
		assert.Equal(t, "localhost:1234", r.URL.Host)
		assert.Equal(t, "/foo", r.URL.Path)
	}

	req, err := http.NewRequest("POST", "http://localhost:8090/foo", strings.NewReader("helloWorld"))
	assert.NoError(t, err)
	w := httptest.NewRecorder()

	// set test aws credentials
	os.Setenv("AWS_ACCESS_KEY_ID", "TEST_ID")
	os.Setenv("AWS_SECRET_ACCESS_KEY", "TEST_KEY")
	os.Setenv("AWS_SECURITY_TOKEN", "TESTSECTOKEN")
	os.Setenv("AWS_SESSION_TOKEN", "TESTSESTOKEN")

	p.awsSigningMiddleware(testHandler).ServeHTTP(w, req)
}
func TestAddress(t *testing.T) {
	type testCase struct {
		expectedAddr string
		config       Config
	}

	testCases := []testCase{
		{
			expectedAddr: "localhost:8080",
			config:       Config{Host: "localhost", Port: 8080},
		},
		{
			expectedAddr: "127.0.0.1:8082",
			config:       Config{Host: "127.0.0.1", Port: 8082},
		},
	}

	for _, c := range testCases {
		assert.Equal(t, c.expectedAddr, c.config.address())
	}
}
