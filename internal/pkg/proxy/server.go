package proxy

import (
	"context"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"net/url"
	"time"

	"github.com/sirupsen/logrus"

	"github.com/aws/aws-sdk-go/aws/credentials"
)

// proxy represents the proxy http server and all its dependencies
type proxy struct {
	server    *http.Server
	awsCreds  *credentials.Credentials
	context   context.Context
	logger    *logrus.Logger
	logWriter *io.PipeWriter
	config    *Config
	targetURL *url.URL
}

// Config stores the proxy configuration
type Config struct {
	Host                    string
	Port                    int
	Service                 string
	Target                  string
	Region                  string
	DisableHTTPSRewrite     bool
	DisableCredentialsCheck bool
}

// NewProxy creates a new proxy
func NewProxy(c context.Context, config *Config) (*proxy, error) {
	logger := logrus.New()
	logger.SetFormatter(&logrus.TextFormatter{})
	w := logger.Writer()

	// parse target url
	var targetURL *url.URL
	if config.Target != "" {
		var err error
		targetURL, err = url.Parse(config.Target)

		if err != nil {
			return nil, err
		}
	}

	// create proxy
	p := proxy{
		server: &http.Server{
			Addr:        config.address(),
			BaseContext: func(net.Listener) context.Context { return c },
			ErrorLog:    log.New(w, "", 0),
		},
		logger:    logger,
		logWriter: w,
		context:   c,
		config:    config,
		targetURL: targetURL,
	}

	// setup middleware
	p.server.Handler = p.loggingMiddleware(
		p.awsSigningMiddleware(nil),
	)

	return &p, nil
}

// ListenAndServe starts the http server and listens for incoming requests
func (p *proxy) ListenAndServe() error {
	go func() {
		p.logger.Infof("listening for incoming requests on %s", p.config.address())

		if p.targetURL != nil {
			p.logger.Infof("running in [reverse proxy mode], as --target %s configured,", p.config.Target)
		} else {
			p.logger.Infof("running in [forward proxy mode], specify a target to switch to reverse proxy mode")
		}

		if err := p.server.ListenAndServe(); err != http.ErrServerClosed {
			p.logger.Fatalf("starting server: %v", err)
		}
	}()

	select {
	case <-p.context.Done():
		p.logger.Info("received signal, shutting down")

		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		if err := p.server.Shutdown(ctx); err != nil {
			return fmt.Errorf("error shutting down: %s", err)
		}
	}

	return nil
}
