package proxy

import (
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
)

// httpLog captures information about an http request
type httpLog struct {
	method          string
	host            string
	url             string
	src             string
	code            int
	userAgent       string
	durationTotalMS int64
	sizeBytes       int
}

// statusResponseWriter captures the response status
type statusResponseWriter struct {
	http.ResponseWriter
	status int
	size   int
}

// WriteHeader satisifies the response writer interface
func (p *statusResponseWriter) WriteHeader(status int) {
	p.status = status
	p.ResponseWriter.WriteHeader(status)
}

// Write satisifies the response writer interface
func (p *statusResponseWriter) Write(b []byte) (int, error) {
	if p.status == 0 {
		p.status = 200
	}
	n, err := p.ResponseWriter.Write(b)
	p.size += n
	return n, err
}

// loggingMiddleware logs http requests traversing the handler
func (p *proxy) loggingMiddleware(h http.HandlerFunc) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		startTime := time.Now()
		sResponseWriter := statusResponseWriter{ResponseWriter: w}

		h(&sResponseWriter, r)

		c := captureHTTPLog(r, &sResponseWriter, startTime)

		p.logger.WithFields(logrus.Fields{
			"method":          c.method,
			"host":            c.host,
			"url":             c.url,
			"src":             c.src,
			"code":            c.code,
			"userAgent":       c.userAgent,
			"durationTotalMS": c.durationTotalMS,
			"sizeBytes":       c.sizeBytes,
		}).Info("proxy request served")
	}
}

// captureHTTPLog crafts the structured log
func captureHTTPLog(req *http.Request, resp *statusResponseWriter, startTime time.Time) *httpLog {
	return &httpLog{
		method:          req.Method,
		host:            req.Host,
		url:             req.URL.String(),
		src:             req.RemoteAddr,
		code:            resp.status,
		userAgent:       req.UserAgent(),
		durationTotalMS: time.Now().Sub(startTime).Milliseconds(),
		sizeBytes:       resp.size,
	}
}
