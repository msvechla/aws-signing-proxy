package proxy

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"time"

	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/defaults"
	"github.com/aws/aws-sdk-go/aws/session"
	v4 "github.com/aws/aws-sdk-go/aws/signer/v4"
	"github.com/aws/aws-sdk-go/service/sts"
)

// awsSigningMiddleware signs http requests via the aws v4 signer
func (p *proxy) awsSigningMiddleware(h http.HandlerFunc) http.HandlerFunc {

	// initialize the AWS credentials
	p.initAWSCredentials()

	proxy := httputil.ReverseProxy{
		ErrorLog: log.New(p.logWriter, "", 0),
		Director: func(req *http.Request) {

			// make a copy of the body and reference it in the original drained request so it can be safely consumed by the signer
			body := []byte{}
			if req.Body != nil {
				var err error
				body, err = ioutil.ReadAll(req.Body)
				if err != nil {
					p.logger.Errorf("reading request body: %s", err)
					return
				}

				bodyCopy := ioutil.NopCloser(bytes.NewBuffer(body))
				req.Body = bodyCopy
			}

			// if we are running in reverse proxy mode, modify the target url
			if p.targetURL != nil {
				modifyReverseProxyTarget(req, p.targetURL)
			}

			// upgrade to tls
			if !p.config.DisableHTTPSRewrite {
				req.URL.Scheme = "https"
			}

			// remove headers that should not get signed
			deleteProxyHeaders(req)

			// refresh the AWS credentials
			if _, err := p.awsCreds.Get(); err != nil {
				p.logger.Fatalf("refreshing AWS credentials: %s", err)
			}

			// sign the request
			signer := v4.NewSigner(p.awsCreds)
			if _, err := signer.Sign(req, bytes.NewReader(body), p.config.Service, p.config.Region, time.Now()); err != nil {
				p.logger.Errorf("signing http request: %s", err)
			}

			// if handlerfunc was passed, call it. Currently this is only needed for test cases
			if h != nil {
				h(nil, req)
			}
		},
	}

	return proxy.ServeHTTP
}

func modifyReverseProxyTarget(req *http.Request, target *url.URL) {
	req.URL.Scheme = target.Scheme
	req.URL.Host = target.Host
	req.Host = target.Host
}

// initAWSCredentials initializes AWS credentials via the default credentials chain
func (p *proxy) initAWSCredentials() {
	config := defaults.Config()
	p.awsCreds = credentials.NewChainCredentials(defaults.CredProviders(config, defaults.Handlers()))
	if _, err := p.awsCreds.Get(); err != nil {
		log.Fatalf("Error initializing AWS credentials: %s", err)
	}

	config.Credentials = p.awsCreds

	// ensure the credentials are valid / not expired
	svc := sts.New(session.New(config))
	input := &sts.GetCallerIdentityInput{}

	if !p.config.DisableCredentialsCheck {
		if _, err := svc.GetCallerIdentity(input); err != nil {
			log.Fatalf("Invalid AWS credentials: %s", err)
		}
	}
}

// deleteProxyHeaders removes all proxy specific headers that will not bet sent to the target and therefor should not be signed as well
func deleteProxyHeaders(req *http.Request) {

	//TODO: this probably requires more sophisticated logic, see Hop-by-hop headers in httputil.ReverseProxy
	headersToDelete := []string{
		"proxy-connection",
	}

	for _, h := range headersToDelete {
		req.Header.Del(h)
	}
}

func (c *Config) address() string {
	return fmt.Sprintf("%s:%d", c.Host, c.Port)
}
